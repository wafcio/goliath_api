require 'grape/exceptions/base'

module Grape
  module Exceptions
    class ValidationErrors < Grape::Exceptions::Base
      def initialize(args = {})
        errors = {}
        args[:errors].each do |validation_error|
          errors[param(validation_error)] ||= []
          errors[param(validation_error)] << validation_error.message
        end

        super message: errors, status: 400
      end

      def param(validation_error)
        translate_attribute(validation_error.param)
      end
    end
  end
end