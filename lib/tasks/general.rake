namespace :bundler do
  task :setup do
    require 'rubygems'
    require 'bundler/setup'
  end
end

task :environment => 'bundler:setup' do
  require 'yaml'
  require 'sequel'
  conf = YAML.load_file('config/database.yml')[ENV['APP_ENV'] || 'development']
  DB = Sequel.connect "#{conf['adapter']}://#{conf['username']}:#{conf['password']}@#{conf['host']}/#{conf['database']}"
end
