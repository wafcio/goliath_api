def route_method(name)
  (7 - name.size).times { |_| name = ' ' + name }
  name
end

desc 'Print out all defined routes'
task :routes => :environment do
  require 'grape'
  require 'grape-entity'
  require './app/api'

  API.routes.each do |route|
    route_path = route.route_path.gsub('(.:format)', '(.json)').gsub(':version', route.route_version)
    puts "#{route_method(route.route_method)} #{route_path}"
    if ENV['DESC'] == 'true'
      puts "#{route.route_description}" if route.route_description
      if route.route_params.is_a?(Hash)
        params = route.route_params.map do |name, desc|
          required = desc.is_a?(Hash) ? desc[:required] : false
          description = desc.is_a?(Hash) ? desc[:description] : desc.to_s
          [ name, required, "   * #{name}: #{description} #{required ? '(required)' : ''}" ]
        end
        if params.any?
          puts " parameters:"
          params.each { |p| puts p[2] }
        end
      end
      puts
    end
  end
end
