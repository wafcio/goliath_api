namespace :db do
  namespace :schema do
    desc 'Dump schema to db/schema.rb'
    task :dump => :environment do
      DB.extension :schema_dumper

      output = "# current version: #{DB[:schema_info].first[:version]}\n\n" + DB.dump_schema_migration(same_db: true)
      File.open(File.join('db', 'schema.rb'), 'w'){ |f| f.write(output) }
    end
  end

  desc 'Run database migrations'
  task :migrate => :environment do
    Sequel.extension :migration

    if ENV['VERSION']
      Sequel::Migrator.apply(DB, 'db/migrates', ENV['VERSION'].to_i)
    else
      Sequel::Migrator.apply(DB, 'db/migrates')
    end

    Rake::Task['db:schema:dump'].invoke
  end

  desc 'Nuke the database (drop all tables)'
  task :nuke => :environment do
    DB.tables.each do |table|
      DB.run("DROP TABLE #{table}")
    end

    File.unlink File.join('db', 'schema.rb')
  end
end
