desc 'Run console'
task :console => :environment do
  require 'pry'
  require 'grape'
  require 'grape-entity'
  require './lib/grape/exceptions/validation_errors'
  require './lib/common_regex'

  ['entities', 'models'].each do |dir|
    Dir["./app/#{dir}/**/*.rb"].each { |f| require f }
  end

  ARGV.clear
  pry
end

desc 'Run console'
task :c => :environment do
  Rake::Task['console'].invoke
end
