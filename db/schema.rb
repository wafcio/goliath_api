# current version: 1

Sequel.migration do
  change do
    create_table(:schema_info) do
      column :version, "integer", :default=>0, :null=>false
    end
    
    create_table(:users) do
      primary_key :id
      column :name, "text", :null=>false
      column :surname, "text", :null=>false
      column :email, "text", :null=>false
      column :phones, "text[]"
    end
  end
end
