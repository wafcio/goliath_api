require 'yaml'
require 'pg'
require 'sequel'
require 'em-postgresql-sequel' unless Goliath.env == :test
require './lib/grape/exceptions/validation_errors'
require './lib/common_regex'

conf = YAML.load_file('config/database.yml')[Goliath.env.to_s]
Sequel.connect "#{conf['adapter']}://#{conf['username']}:#{conf['password']}@#{conf['host']}/#{conf['database']}"

['entities', 'models'].each do |dir|
  Dir["./app/#{dir}/**/*.rb"].each { |f| require f }
end
