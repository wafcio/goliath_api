require 'rubygems'
require 'bundler/setup'
require 'goliath'
require 'grape'
require 'grape-entity'
require './app/api'

class Application < Goliath::API
  def response(env)
    ::API.call(env)
  end
end
