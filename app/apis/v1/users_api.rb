module APIv1
  class UsersApi < Grape::API
    version 'v1', using: :path

    helpers do
      def user_params
        params.to_hash.select { |key, _| User.whitelist_attributes.include?(key.to_sym) }
      end
    end

    resource :users do
      # GET /users.json
      desc 'Returns list of all users.'
      get '/' do
        present User.all, with: ::UserEntity
      end

      # POST /users.json
      desc 'Create user.'
      post '/' do
        user = User.new(user_params)
        if user.valid?
          present user.save, with: ::UserEntity
        else
          error! user.errors, 400
        end
      end

      # GET /users/1.json
      desc 'Returns user by ID'
      get '/:id' do
        user = User.find(id: params['id'])
        if user
          present user, with: ::UserEntity
        else
          error! 'Not Found', 404
        end
      end

      # PATCH /users/1.json
      desc 'Update user by ID'
      patch '/:id' do
        user = User.find(id: params['id'])
        if user
          user.set(user_params)
          if user.valid?
            present user.save, with: ::UserEntity
          else
            error! user.errors, 400
          end
        else
          error! 'Not Found', 404
        end
      end

      # DELETE /users/1.json
      desc 'Delete user by ID'
      delete '/:id' do
        user = User.find(id: params['id'])
        if user
          present user.destroy, with: ::UserEntity
        else
          error! 'Not Found', 404
        end
      end
    end
  end
end


