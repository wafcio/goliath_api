class UserEntity < Grape::Entity
  expose :id
  expose :name
  expose :surname
  expose :email
end
