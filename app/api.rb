['apis'].each do |dir|
  Dir["./app/#{dir}/**/*.rb"].each { |f| require f }
end

class API < Grape::API
  format :json
  default_format :json

  mount APIv1::UsersApi
end
