class User < Sequel::Model
  plugin :validation_helpers

  def self.whitelist_attributes
    [:name, :surname, :email]
  end

  def validate
    super

    validates_presence [:name, :surname, :email]
    validates_unique :email
    validates_format CommonRegex::EMAIL, :email
  end
end
