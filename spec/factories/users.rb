FactoryGirl.define do
  factory(:user) do
    name 'tester'
    surname 'tester'
    sequence(:email) {|n| "person#{(n + Kernel.rand).to_s}@example.com" }
  end
end
