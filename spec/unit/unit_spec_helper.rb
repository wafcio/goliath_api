require './spec/spec_helper'

require 'rspec_sequel_matchers'
require 'grape-entity-matchers'

RSpec.configure do |config|
  config.include RspecSequel::Matchers
  config.include GrapeEntityMatchers::RepresentMatcher
end
