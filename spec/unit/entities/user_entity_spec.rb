require './spec/unit/unit_spec_helper'

describe UserEntity do
  subject(:entity){ UserEntity }

  it { should represent(:name) }
  it { should represent(:surname) }
  it { should represent(:email) }
end
