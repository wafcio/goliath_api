require './spec/unit/unit_spec_helper'

describe User do
  it { should have_column :name, type: String }
  it { should have_column :surname, type: String }
  it { should have_column :email, type: String }

  it { should validate_presence :name }
  it { should validate_presence :surname }
  it { should validate_presence :email }
  it { should validate_unique :email }

  it { should validate_format CommonRegex::EMAIL, :email }
end
