module Sequel
  class Model
    module InstanceMethods
      def save!
        save
      end
    end
  end
end