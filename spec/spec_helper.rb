require 'factory_girl'
require 'yaml'
require 'sequel'
require 'grape'
require 'grape-entity'
conf = YAML.load_file('config/database.yml')['test']
DB = Sequel.connect "#{conf['adapter']}://#{conf['username']}:#{conf['password']}@#{conf['host']}/#{conf['database']}"

require './spec/support/sequel_patch'

require './lib/grape/exceptions/validation_errors'
require './lib/common_regex'

['entities', 'models'].each do |dir|
  Dir["./app/#{dir}/**/*.rb"].each { |f| require f }
end

FactoryGirl.find_definitions

RSpec.configure do |config|
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.run_all_when_everything_filtered = true
  config.filter_run :focus

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'
end
