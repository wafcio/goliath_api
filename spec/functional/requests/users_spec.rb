require './spec/functional/functional_spec_helper'
#require File.join(File.dirname(__FILE__), '../', 'app')

describe 'Users resource' do
  let(:user) { FactoryGirl.create(:user) }
  let(:user_hash) { { name: user.name, surname: user.surname, email: user.email } }

  describe 'User list' do
    let(:json) { generate_normalized_json([user_hash]) }

    before do
      user
    end

    it 'should return Array' do
      with_api Application do
        get_request path: '/v1/users' do |c|
          c.response.should have_json_type(Array)
        end
      end
    end

    it 'should return correct json' do
      with_api Application do
        get_request path: '/v1/users' do |c|
          c.response.should be_json_eql(json)
        end
      end
    end
  end

  describe 'Create user' do
    let(:user_hash) { { name: 'tester', surname: 'tester', email: "tester#{Date.today}@example.com" } }
    let(:json) { generate_normalized_json(user_hash) }

    it 'should return error hash' do
      with_api Application do
        post_request path: '/v1/users' do |c|
          parse_json(c.response).keys.sort.should == ['name', 'surname', 'email'].sort
        end
      end
    end

    it 'should return correct json' do
      with_api Application do
        post_request path: "/v1/users?#{to_query(name: 'tester', surname: 'tester', email: "tester#{Date.today}@example.com")}" do |c|
          c.response.should be_json_eql(json)
        end
      end
    end
  end

  describe 'Show user' do
    let(:error_json) { generate_normalized_json(error: 'Not Found') }
    let(:json) { generate_normalized_json(user_hash) }

    it 'should return error Not Found when user not exists' do
      with_api Application do
        get_request path: '/v1/users/12345' do |c|
          c.response.should be_json_eql(error_json)
        end
      end
    end

    it 'should return correct json' do
      user

      with_api Application do
        get_request path: "/v1/users/#{user.id}" do |c|
          c.response.should be_json_eql(json)
        end
      end
    end
  end

  describe 'Update user' do
    let(:error_json) { generate_normalized_json(error: 'Not Found') }
    let(:json) { generate_normalized_json(user_hash) }

    it 'should return error Not Found when user not exists' do
      with_api Application do
        patch_request path: '/v1/users/12345' do |c|
          c.response.should be_json_eql(error_json)
        end
      end
    end

    it 'should return error hash' do
      user

      with_api Application do
        patch_request path: "/v1/users/#{user.id}?#{to_query(name: '')}" do |c|
          parse_json(c.response).keys.sort.should == ['name'].sort
        end
      end
    end

    it 'should return correct hash' do
      user

      with_api Application do
        patch_request path: "/v1/users/#{user.id}" do |c|
          c.response.should be_json_eql(json)
        end
      end
    end
  end

  describe 'Delete user' do
    let(:error_json) { generate_normalized_json(error: 'Not Found') }
    let(:json) { generate_normalized_json(user_hash) }

    it 'should return error Not Found when user not exists' do
      with_api Application do
        delete_request path: '/v1/users/12345' do |c|
          c.response.should be_json_eql(error_json)
        end
      end
    end

    it 'should return correct json' do
      user

      with_api Application do
        delete_request path: "/v1/users/#{user.id}" do |c|
          c.response.should be_json_eql(json)
        end
      end
    end
  end
end