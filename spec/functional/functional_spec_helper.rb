require './spec/spec_helper'

require 'goliath/test_helper'
require 'json_spec'
require 'database_cleaner'
require './spec/functional/support/database_setup'
require './server'

Dir["./spec/functional/support/**/*.rb"].each { |f| require f }

db = DatabaseSetup.new.run

Goliath.env = :test

RSpec.configure do |config|
  config.include Goliath::TestHelper
  config.include JsonSpec::Helpers
  config.include ToQuery

  config.before(:suite) do
    db.cleaner = DatabaseCleaner[:sequel, { connection: db.connection }]
    db.cleaner.strategy = :truncation
  end

  config.before(:each) do
    db.cleaner.strategy = :truncation
    db.cleaner.start
  end

  config.after(:each) do
    begin
      db.cleaner.clean
    rescue
      sleep 5
      db.cleaner.clean
    end
  end
end
