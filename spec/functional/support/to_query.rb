require 'addressable/uri'

module ToQuery
  def to_query(data)
    uri = Addressable::URI.new
    uri.query_values = data
    uri.query
  end
end
