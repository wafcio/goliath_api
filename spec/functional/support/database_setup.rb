require 'yaml'
require 'sequel'

class DatabaseSetup
  attr_accessor :cleaner

  def initialize
    Sequel.extension :migration
  end

  def config
    @config ||= YAML.load_file('config/database.yml')['test']
  end

  def run
    connection
    drop_tables
    run_migrations
    self
  end

  def connection
    @connection ||= Sequel.connect "#{config['adapter']}://#{config['username']}:#{config['password']}@#{config['host']}/#{config['database']}"
  end

  def run_migrations
    Sequel::Migrator.apply(connection, './db/migrates')
  end

  def drop_tables
    connection.tables.each do |table|
      connection.run("DROP TABLE #{table}")
    end
  end

  #def clean_database
  #  db_connect.tables.reject{ |t| t == :schema_info }.each{ |t| puts db_connect[t].all }
  #end
end
